# KaralundiComponents

[![MIT License][license-badge]][license]

## Description

React Native Karalundi components customizables.

- [Loader](#loader)
- [ActivityLoader](#activityloader)
- [Input](#input)

## Install

```
$ npm install git+https://gitlab.com/residenciaskaralundi/karalundiComponents.git
```

## Demo
## Loader

### Examples


```jsx
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Loader } from 'karalundiComponents';
export default class Example extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <Text>Karalundi Componets</Text>
                <Loader isVisible={true} text='Cargando' textColor='#F3F' colorActivityIndicator='#FF4F' />
            </View>
        );
    }
}

```

### Documentation


| Prop                     | Type      | Description                                    | Default | Usage|
| :----------------------- | :-------: | :--------------------------------------------: | :------ | :------|
| isVisible                | `boolean` | Show / Hide Loader                      | false   |optional|
| text             | `string` | Text Loader                       | ''   |optional|
| backgroundColor  | `string`  | Background color Loader                          | `#000000CC`  |optional|
| colorActivityIndicator | `string`  | Color of activity indicator                        | `#000000`  |optional|
| textColor      | `string`    | Color of text              | `#000000`    |optional|


## ActivityLoader

### Examples

```jsx
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { ActivityLoader } from 'karalundiComponents';
export default class Example extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <Text>Karalundi Componets</Text>
                <ActivityLoader isVisible={true} text='Cargando' textColor='#FFF' colorActivityIndicator='#2C3E50' />
            </View>
        );
    }
}

```

### Documentation


| Prop                     | Type      | Description                                    | Default | Usage|
| :----------------------- | :-------: | :--------------------------------------------: | :------ | :------|
| isVisible                | `boolean` | Show / Hide Loader                      | false   |optional|
| text             | `string` | Text Loader                       | ''   |optional|
| backgroundColor  | `string`  | Background color Loader                          | `#000000CC`  |optional|
| colorActivityIndicator | `string`  | Color of activity indicator                        | `#000000`  |optional|
| textColor      | `string`    | Color of text              | `#000000`    |optional|

## Input

### Examples

```jsx
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Input } from 'karalundiComponents';
export default class Example extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <Text>Karalundi Componets</Text>
                <Input text="Captura de datos" placeholder="Ingrese un dato" />
            </View>
        );
    }
}

```

### Documentation

> Also receives all `Input` [(**react-native-elements**)](https://react-native-elements.github.io/react-native-elements/docs/input.html) props.

| Prop                     | Type      | Description                                    | Default | Usage|
| :----------------------- | :-------: | :--------------------------------------------: | :------ | :------|
| text             | `string` | Text in Top Input                       |    |required|
| placeholder  | `string`  | Placeholder input                          |   |optional|
| iconName | `string`  | Icon Name                        |   |optional|
| iconType      | `string`    | Icon Type              |     |optional|
| name             | `string` | Name (id) Input                       |    |optional|
| onChange  | `function`  | Function                          |   |optional|
| color | `string`  | Color label input                        | `#3F515A`  |optional|
| borderColor      | `string`    | Border color of input              | `#000000`    |optional|
| iconColor      | `string`    | Icon color of icon in input              | `#3F515A`    |optional|

## Scripts


## License

MIT © [José Arturo Olvera](https://gitlab.com/residenciaskaralundi/karalundiComponents/blob/development/LICENSE)


[npm-badge]: https://img.shields.io/badge/npm->=%203.10.8-blue.svg?style=flat-square
[npm]: https://docs.npmjs.com/
[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[license]: https://gitlab.com/residenciaskaralundi/karalundiComponents/blob/development/LICENSE

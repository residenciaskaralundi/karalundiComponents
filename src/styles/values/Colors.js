const colors = {
    loaderDefaultColor: '#000000cc',
    transparent: 'rgba(0,0,0,0)',
    white: '#FFFFFF',
    grey: '#3f515A',
    red: '#AE2A2D',
    lightGrey: '#CCCCCC',
    black: '#000000',
    wetAsphalt:'#34495E',
    midnightBlue: '#2C3E50',
    lightBlue: '#2980b9',
    green: '#27AE60',
    yellow: '#FFC312',
};
export default colors;
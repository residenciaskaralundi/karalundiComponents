import { StyleSheet } from 'react-native';
import { normalize } from './globalStyles';

export const inputStyles = StyleSheet.create({
    input: {
        fontSize: normalize(12),
        marginLeft: normalize(5),
    },
    inputOutContainer: {
        marginTop: normalize(5),
        marginBottom: normalize(5),
    },
    inputInContainer: {
        borderWidth: normalize(1),
        borderRadius: normalize(15),
    },
    inputLabel: {
        fontSize: normalize(12),
        fontWeight: 'normal',
        marginLeft: normalize(10),
        marginBottom: normalize(4),
    },
});
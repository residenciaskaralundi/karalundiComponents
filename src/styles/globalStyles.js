import { Dimensions, Platform, PixelRatio, StyleSheet } from 'react-native';
const { width, height, } = Dimensions.get('window');
const scale = width / 320;

export function normalize(size) {
    const newSize = size * scale;
    if (Platform.OS === 'ios') {
        return Math.round(PixelRatio.roundToNearestPixel(newSize));
    }
    else {
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
    }
}

export const globalStyles = StyleSheet.create({
    loaderContainer: {
        flex: 1,
        bottom: 0,
        top: 0,
        left: 0,
        right: 0,
        flexDirection: 'column',
        justifyContent: 'space-around',
        position: 'absolute',
        alignItems: 'center',
    },
    loaderText: {
        fontWeight: 'bold',
        marginTop: 20,
        padding: 30,
        textAlign: 'center',
    },
    button1: {
        borderRadius: 15,
    },
    button1Title: {
        color: '#fff',
        fontSize: normalize(13),
    },
    textButton: {
    },
    textButtonTitle: {
        fontWeight: 'bold',
        fontSize: normalize(13),
    },
});
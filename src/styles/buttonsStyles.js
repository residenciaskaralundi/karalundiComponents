import { StyleSheet } from 'react-native';
import { normalize } from './globalStyles';

export const buttonsStyles = StyleSheet.create({
    simpleButton: {
        borderRadius: normalize(15),
    },
    simpleButtonTitle: {
        fontSize: normalize(13),
    },
    textButton: {
    },
    textButtonTitle: {
        fontWeight: 'bold',
        fontSize: normalize(13),
    },
});
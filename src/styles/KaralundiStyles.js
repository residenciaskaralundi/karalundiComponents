import { loaderStyles } from './loaderStyles';
import { globalStyles } from './globalStyles';
import { inputStyles } from './inputStyles';
import { buttonsStyles } from './buttonsStyles';
import { Colors } from './Values';
export { loaderStyles, globalStyles, Colors, inputStyles, buttonsStyles };

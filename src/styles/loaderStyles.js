import { StyleSheet } from 'react-native';
import { normalize } from './globalStyles';

export const loaderStyles = StyleSheet.create({
    loaderContainer: {
        flex: 1,
        bottom: 0,
        top: 0,
        left: 0,
        right: 0,
        flexDirection: 'column',
        justifyContent: 'space-around',
        position: 'absolute',
        alignItems: 'center',
    },
    loaderText: {
        fontWeight: 'bold',
        marginTop: normalize(20),
        padding: normalize(30),
        textAlign: 'center',
    },
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
    modalContainer: {
        alignItems: 'center',
        margin: normalize(30),
    },
});
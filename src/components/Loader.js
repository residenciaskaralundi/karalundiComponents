import React, { Component } from 'react';
import { ActivityIndicator, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { loaderStyles, Colors } from '../styles/KaralundiStyles';

export default class Loader extends Component {

    static propTypes = {
        isVisible: PropTypes.bool,
        text: PropTypes.string,
        backgroundColor: PropTypes.string,
        colorActivityIndicator: PropTypes.string,
        textColor: PropTypes.string,
    }

    static defaultProps = {
        isVisible: false,
        text: '',
        backgroundColor: Colors.loaderDefaultColor,
        colorActivityIndicator: Colors.black,
        textColor: Colors.black,
    }

    render() {
        if (this.props.isVisible){
            let backgroundColor = this.props.backgroundColor;
            let color = this.props.textColor;
            return (
                <View style={[loaderStyles.loaderContainer, {backgroundColor}]}>
                    <View>
                        <ActivityIndicator size='large' color={this.props.colorActivityIndicator} />
                        <Text style={[loaderStyles.loaderText, {color}]}>
                            {this.props.text}
                        </Text>
                    </View>
                </View>
            );
        }
        else {
            return null;
        }
    }
}


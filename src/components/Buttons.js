import React, { Component } from 'react';
import { Linking, Alert } from 'react-native';
import { Button as RNEButton, Icon } from 'react-native-elements';
import {buttonsStyles, Colors} from '../styles/KaralundiStyles';
import PropTypes from 'prop-types';

export class Button extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        onPress: PropTypes.func.isRequired,
        backgroundColor: PropTypes.string,
        color: PropTypes.string,
    }

    static defaultProps = {
        backgroundColor: Colors.lightBlue,
        color: Colors.white,
    }

    render() {
        let backgroundColor = this.props.backgroundColor;
        let color = this.props.color;
        return (
            <RNEButton
                {...this.props}
                type='solid'
                buttonStyle={[buttonsStyles.simpleButton, {backgroundColor: backgroundColor, }]}
                titleStyle={[buttonsStyles.simpleButtonTitle, {color: color, }]}
                title={this.props.title}
                onPress={ this.props.onPress } />
        );
    }
}

export class TextButton extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        iconName: PropTypes.string,
        iconType: PropTypes.string,
        color: PropTypes.string,
    }

    renderIcon = () => {
        if (this.props.iconName != ''){
            return (
                <Icon
                    iconStyle={{ color: this.props.color, }}
                    type={this.props.iconType || ''}
                    name={this.props.iconName || ''} />
            );
        }
        return null;
    }

    render() {
        return (
            <RNEButton
                {...this.props}
                type='clear'
                buttonStyle={buttonsStyles.textButton}
                titleStyle={[buttonsStyles.textButtonTitle, {color: this.props.color, }, ]}
                title={this.props.title}
                icon={ this.renderIcon() } />
        );
    }
}

export class UrlButton extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        iconName: PropTypes.string,
        iconType: PropTypes.string,
        color: PropTypes.string,
        url: PropTypes.string,
        hidden: PropTypes.bool,
    }

    static defaultProps ={
        title: 'Url Button',
        iconName: '',
        iconType: '',
        color: 'blue',
        hidden: false,
    };

    openUrl = (url) => {
        //Verifica que el dispositivo cuente con un navegador web
        Linking.canOpenURL(url)
            .then((supported) => {
                if (!supported) {
                    this.browserAlert('Aviso', 'Necesitas un navegador para acceder.');
                }
                else {
                    return Linking.openURL(url);
                }
            })
            .catch((err) => console.error('Ocurrío un error.', err));
    }

    browserAlert = (title, msj) => {
        Alert.alert(
            title,
            msj,
            [
                {text: 'Enterado', onPress: () => { }, style: 'cancel', },
            ],
            { cancelable: true, }
        );
    }

    renderIcon = () => {
        if (this.props.iconName != ''){
            return (
                <Icon
                    iconStyle={{ color: this.props.color, }}
                    type={this.props.iconType || ''}
                    name={this.props.iconName || ''} />
            );
        }
        return null;
    }

    render() {
        if (!this.props.hidden) {
            return (
                <RNEButton
                    {...this.props}
                    type='clear'
                    buttonStyle={buttonsStyles.textButton}
                    titleStyle={[buttonsStyles.textButtonTitle, {color: this.props.color, }, ]}
                    title={this.props.title}
                    icon={ this.renderIcon() }
                    onPress={ () => {
                        this.openUrl(this.props.url);
                    }} />
            );
        }
        else {
            return null;
        }
    }
}

import React, {Component} from 'react';
import { View, Modal, ActivityIndicator, Text} from 'react-native';
import PropTypes from 'prop-types';
import { loaderStyles, Colors } from '../styles/KaralundiStyles';

export default class ActivityLoader extends Component {

    static propTypes = {
        isVisible: PropTypes.bool,
        text: PropTypes.string,
        backgroundColor: PropTypes.string,
        colorActivityIndicator: PropTypes.string,
        textColor: PropTypes.string,
    }

    static defaultProps = {
        isVisible: false,
        text: '',
        backgroundColor: Colors.loaderDefaultColor,
        colorActivityIndicator: Colors.black,
        textColor: Colors.black,
    }

    render(){
        let backgroundColor = this.props.backgroundColor;
        let color = this.props.textColor;
        return (
            <Modal transparent={true} animationType={'none'} visible={this.props.isVisible} onRequestClose={ () => {} }>
                <View style={[loaderStyles.modalBackground, {backgroundColor}]}>
                    <View style={loaderStyles.modalContainer}>
                        <ActivityIndicator size='large' color={this.props.colorActivityIndicator} />
                        <Text style={[loaderStyles.loaderText, {color}]}>
                            {this.props.text}
                        </Text>
                    </View>
                </View>
            </Modal>
        );
    }
}
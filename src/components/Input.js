import React, { Component } from 'react';
import { Input as RNEInput, Icon } from 'react-native-elements';
import {inputStyles, Colors} from '../styles/KaralundiStyles';
import PropTypes from 'prop-types';

export default class Input extends Component {

    static propTypes = {
        text: PropTypes.string.isRequired,
        placeholder: PropTypes.string,
        iconName: PropTypes.string,
        iconType: PropTypes.string,
        name: PropTypes.string,
        onChange: PropTypes.func,
        color: PropTypes.string,
        borderColor: PropTypes.string,
        iconColor: PropTypes.string,
    }

    static defaultProps = {
        color: Colors.grey,
        borderColor: Colors.black,
        iconColor: Colors.grey,
    }

    handleChange = (event) => {
        if (typeof(this.props.onChange) == 'function'){
            this.props.onChange(this.props.name, event.nativeEvent.text);
        }
    }

    handleBlur = (event) =>{
        let string = event.nativeEvent.text;
        if (string != undefined & string != ''){
            this.props.onChange(this.props.name, string.trim());
        }
    }

    render() {
        let borderColor = this.props.borderColor;
        let color = this.props.color;
        let iconColor = this.props.iconColor;
        return (
            <RNEInput
                {...this.props}
                placeholder={this.props.placeholder}
                label={this.props.text}
                containerStyle={inputStyles.inputOutContainer}
                inputContainerStyle={[inputStyles.inputInContainer, {borderColor}]}
                labelStyle={[inputStyles.inputLabel, {color: color, }]}
                inputStyle={inputStyles.input}
                onChange={this.handleChange}
                onEndEditing={this.handleBlur}
                leftIcon={
                    <Icon
                        type={this.props.iconType}
                        iconStyle={{ color: iconColor, }}
                        name={this.props.iconName} />
                }
            />
        );
    }

}
